package com.sourceit;

public class Main {

    public static void main(String[] args) {
	    Updater updater = new Updater();
        Reader reader = new Reader();

        updater.start();
        reader.start();
    }
}
