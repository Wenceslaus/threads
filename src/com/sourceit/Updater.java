package com.sourceit;

import java.security.SecureRandom;

/**
 * Created by wenceslaus on 05.04.17.
 */
public class Updater extends Thread {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(3_000);
            } catch (InterruptedException e) {
                return;
            }

            Storage.getInstance().setValue(randomString());
        }
    }

    private String randomString() {
        StringBuilder sb = new StringBuilder(16);
        for (int i = 0; i < 16; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
}
