package com.sourceit;

/**
 * Created by wenceslaus on 05.04.17.
 */
public class Storage {

    private static Storage instance;

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    String value;

    private Storage() {
    }


    public synchronized String getValue() {
        return value;
    }

    public synchronized void setValue(String value) {
        this.value = value;
    }
}
