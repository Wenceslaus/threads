package com.sourceit;

/**
 * Created by wenceslaus on 05.04.17.
 */
public class Reader extends Thread {
    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(1_000);
            } catch (InterruptedException e) {
                return;
            }

            System.out.println("value: " + Storage.getInstance().getValue());
        }
    }
}
